import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, ImageBackground } from 'react-native';

import Login from './login';
import Biodata from './Biodata';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor='#A0522D'
          barstyle='light-content'
          />
          <Biodata />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#A0522D',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
