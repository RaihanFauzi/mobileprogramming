import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Logo from './Logo';
import Form from './form';
export default class login extends Component {
    render() {
        return(
            <View style = {StyleSheet.container}>
                <Logo />
                <Form />
                <View style = {styles.signupTextCont}>
                <Text style={styles.signupText}>Not a remember?</Text>
                <Text style={styles.signupButton}>Sign Up Now</Text> 
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
          flex: 1,
          backgroundColor: '#0F1847',
          alignItems: 'center',
          justifyContent: 'center'
        },
    signupTextCont: {
            flexGrow: 1,
            alignItems: 'flex-end',
            justifyContent: 'center',
            marginVertical: 16,
            flexDirection: 'row'
        },
    signupText: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16
    },
    signupButton: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '500'
    }
});