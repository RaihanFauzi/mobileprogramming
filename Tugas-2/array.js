//1.Range
    function range(starNum, finishNum){
        var ar =[];
        if(starNum == null || finishNum == null){
            return "-1";

        }else{
        if(starNum<finishNum){
            for(var i = starNum; i <= finishNum; i++){
                ar.push(i);
        }
        }else{
            for(var i = starNum; i >= finishNum; i--){
                ar.push(i);
            }
        }
        return ar;
    }
}
    console.log('1.Range');
    console.log(range(1, 10))
    console.log(range(1))
    console.log(range(11, 18))
    console.log(range(54, 56))
    console.log(range())
    console.log('')

//No.2 Range Step
function rangeWithStep(starNum, finishNum, step){
    var ar = [];
    if(starNum == null || finishNum == null){
            return "-1";
    }else{
        if(starNum < finishNum){
        for(var i = starNum; i <= finishNum; i+=step){
            ar.push(i);
        }
    }else{
        for(var i = starNum; i >= finishNum; i-=step){
            ar.push(i);
        }
    }
    return ar;
    }
}
console.log('+----------------------------+'); 
console.log('2.Range Step'); 
console.log('+----------------------------+');
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log('')

//No.3 Sum of Range
function sum(starNum, finishNum, step){
    var ar = 0;
    if(starNum == null && finishNum == null){
        return 0;
    }
    else if(starNum == null){
        return finishNum;
    }
    else if(finishNum == null){
        return starNum;
    }
    else{
        if(step == null){
            step = 1;
    }
    if(starNum < finishNum){
        for(var i = starNum; i <= finishNum; i +=step){
            ar +=i;
    }
    }else{
        for(var i = starNum; i >= finishNum; i -= step){
            ar +=i;
        }   
    }
    return ar;
    }
    }

console.log('+----------------------------+'); 
console.log('3.Sum of Range'); 
console.log('+----------------------------+');
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2)) 
console.log(sum(1))
console.log(sum()) 
console.log('') 
//No.4 Array Multidimensi
function dataHandling(biodata){
    for(var i = 0; i < biodata.length; i++){
    console.log("Nomor ID : " + biodata[i][0]);
    console.log("Nama Lengkap : " + biodata [i][1]);
    console.log("TTL : " + biodata[i][2] + " " + biodata[i][3]);
    console.log("Hobi : " + biodata[i][4]);
    console.log("");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input));
console.log("")

//5. Balik Kata
function balikkata(kata){
    return kata.split("").reverse().join("");
}
console.log(balikkata("Kasur Rusak"));
console.log(balikkata("informatika"));
console.log(balikkata("Haji Ijah"));
console.log(balikkata("racecar"));
console.log(balikkata("I am Humanikers"));
console.log("")

//6.
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Memvbaca"];

function dataHandling2(biodata){
     input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provisinsi Bandar Lampung");
     input.splice(4, 1, "Pria", "SMA Internasional Metro");
     console.log(input)

     var inputSplit = input[3].split("/")
     var inputJoin = inputSplit.join("-")
     var bulan = inputJoin[3] + inputJoin[4]

     switch(bulan){
        case '01': console.log('Januari');
            break;
        case '02': console.log('Februari');
            break;
        case '03': console.log('Maret');
            break;
        case '04': console.log('April');
            break;
        case '05': console.log('Mei');
            break;
        case '06': console.log('Juni');
            break;
        case '07': console.log('Juli');
            break;
        case '08': console.log('Agustus');
            break;
        case '09': console.log('September');
            break;
        case '10' : console.log('Oktober');
            break;
        case '11': console.log('November');
            break;
        case '12': console.log('Desember');
            break;
     }
     
     var inputShortDes = inputSplit.sort(function (a,b){ return b - a});
     console.log(inputShortDes)

     var tes = input[3].split("/")
     console.log(tes.join("-"))

     console.log(input[1].toString().slice(0, 14))

     return biodata
}
dataHandling2();

